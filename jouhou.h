/*****************************************************************************
 *  jouhou.h                                                                 *
 *  A homegrown software based 3D engine.                                    *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#ifndef _JOUHOU_H__
#define _JOUHOU_H__

#ifdef __cplusplus
extern "C" {
#endif

#define JOUHOU_TRUE			(1)
#define JOUHOU_FALSE			(0)

#define	JOUHOU_NONE			(0)

#define JOUHOU_WIREFRAME		(1)
#define JOUHOU_FILLED			(2)

#define JOUHOU_QUAD			(1)
#define JOUHOU_TRIANGLE			(2)
#define JOUHOU_LINE			(3)
#define JOUHOU_POINT			(4)

#define JOUHOU_ALL_INFO			(1)
#define JOUHOU_ZCLIP_INFO		(2)
#define JOUHOU_SCALE_INFO		(3)
#define JOUHOU_TRANSLATE_INFO		(4)
#define JOUHOU_ROTATE_INFO		(5)

int  Jouhou_Init(int x, int y, int stereoscopic);
void Jouhou_Begin(int type);
void Jouhou_End();
void Jouhou_Render();
void Jouhou_ClearVertices();
void Jouhou_Identity();
void Jouhou_PushInfo(int type);
void Jouhou_PopInfo(int type);
void Jouhou_DrawMode(int mode);

void Jouhou_ZClip2d(int on, double znear, double zfar);
#define Jouhou_ZClip2f(on, zn, zf)	Jouhou_ZClip2d((on), (double)(zn), (double)(zf))
#define Jouhou_ZClip2i(on, zn, zf)	Jouhou_ZClip2d((on), (double)(zn), (double)(zf))
#define Jouhou_ZClip(on, zn, zf)	Jouhou_ZClip2d((on), (zn), (zf))

int  Jouhou_Vertex3d(double x, double y, double z);
#define Jouhou_Vertex3f(x, y, z)	Jouhou_Vertex3d((double)(x), (double)(y), (double)(z))
#define Jouhou_Vertex3i(x, y, z)	Jouhou_Vertex3d((double)(x), (double)(y), (double)(z))
#define Jouhou_Vertex3dv(v)		Jouhou_Vertex3d((v)[0], (v)[1], (v)[2])
#define Jouhou_Vertex3fv(v)		Jouhou_Vertex3f((v)[0], (v)[1], (v)[2])
#define Jouhou_Vertex3iv(v)		Jouhou_Vertex3i((v)[0], (v)[1], (v)[2])
#define Jouhou_Vertex(v)		Jouhou_Vertex3dv((v))

void Jouhou_Multiply3d(int vert, double x, double y, double z);
#define Jouhou_Multiply3f(v, x, y, z)	Jouhou_Multiply3d((v), (double)(x), (double)(y), (double)(z))
#define Jouhou_Multiply3i(v, x, y, z)	Jouhou_Multiply3d((v), (double)(x), (double)(y), (double)(z))
#define Jouhou_Multiply3dv(v, m)	Jouhou_Multiply3d((v), (m)[0], (m)[1], (m)[2])
#define Jouhou_Multiply3fv(v, m)	Jouhou_Multiply3f((v), (m)[0], (m)[1], (m)[2])
#define Jouhou_Multiply3iv(v, m)	Jouhou_Multiply3i((v), (m)[0], (m)[1], (m)[2])
#define Jouhou_Multiply(v, m)		Jouhou_Multiply3dv((v), (m))

void Jouhou_Scale3d(double x, double y, double z);
#define Jouhou_Scale3f(x, y, z)		Jouhou_Scale3d((double)(x), (double)(y), (double)(z))
#define Jouhou_Scale3i(x, y, z)		Jouhou_Scale3d((double)(x), (double)(y), (double)(z))
#define Jouhou_Scale3dv(v)		Jouhou_Scale3d((v)[0], (v)[1], (v)[2])
#define Jouhou_Scale3fv(v)		Jouhou_Scale3f((v)[0], (v)[1], (v)[2])
#define Jouhou_Scale3iv(v)		Jouhou_Scale3i((v)[0], (v)[1], (v)[2])
#define Jouhou_Scale(v)			Jouhou_Scale3dv((v))

void Jouhou_Translate3d(double x, double y, double z);
#define Jouhou_Translate3f(x, y, z)	Jouhou_Translate3d((double)(x), (double)(y), (double)(z))
#define Jouhou_Translate3i(x, y, z)	Jouhou_Translate3d((double)(x), (double)(y), (double)(z))
#define Jouhou_Translate3dv(v)		Jouhou_Translate3d((v)[0], (v)[1], (v)[2])
#define Jouhou_Translate3fv(v)		Jouhou_Translate3f((v)[0], (v)[1], (v)[2])
#define Jouhou_Translate3iv(v)		Jouhou_Translate3i((v)[0], (v)[1], (v)[2])
#define Jouhou_Translate(v)		Jouhou_Translate3dv((v))

void Jouhou_Rotate3d(double x, double y, double z);
#define Jouhou_Rotate3f(x, y, z)	Jouhou_Rotate3d((double)(x), (double)(y), (double)(z))
#define Jouhou_Rotate3i(x, y, z)	Jouhou_Rotate3d((double)(x), (double)(y), (double)(z))
#define Jouhou_Rotate3dv(v)		Jouhou_Rotate3d((v)[0], (v)[1], (v)[2])
#define Jouhou_Rotate3fv(v)		Jouhou_Rotate3f((v)[0], (v)[1], (v)[2])
#define Jouhou_Rotate3iv(v)		Jouhou_Rotate3i((v)[0], (v)[1], (v)[2])
#define Jouhou_Rotate(v)		Jouhou_Rotate3dv((v))

void Jouhou_Color1i(int color);

#ifdef __cplusplus
};
#endif

#endif /* _JOUHOU_H__ */
