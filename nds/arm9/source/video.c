/*****************************************************************************
 *  video.c                                                                  *
 *  Video Abstraction functions.                                             *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#include <nds.h>
#include "video.h"

int SCREEN_W, SCREEN_H;
#define BACK_BUFFER_SIZE	(256 * 192)
static u16 *bgData;
static u16 SCREEN_DATA[BACK_BUFFER_SIZE];
static int video_locked = 1;

#define max(a,b) (((a) > (b)) ? (a) : (b))
#define min(a,b) (((a) < (b)) ? (a) : (b))
#define m_abs(a) (((a)<0) ? -(a) : (a))
#define sign(a) (((a)<0) ? -1 : (a)>0 ? 1 : 0)

int Video_Init(int w, int h)
{
	SCREEN_W	= w;
	SCREEN_H	= h;

	bgData = VRAM_A;
	video_locked = 0;
	int i;
	for(i = 0; i < BACK_BUFFER_SIZE; i++) {
		SCREEN_DATA[i] = 0x0000;
	}
	swiWaitForVBlank();
	return 0;
}	

void Video_Quit()
{

}

static void put_pixel(u16* scrn, s16 x, s16 y, u16 color)
{
	if((x >= SCREEN_W) || (y >= SCREEN_H) || (x < 0) || (y < 0))
		return;
	scrn[x + (SCREEN_W * y)] = color | 0x8000;
}

static int draw_line(u16* scrn, s16 x1, s16 y1, s16 x2, s16 y2, u16 color)
{
	int d;
	int x;
	int y;
	int ax;
	int ay;
	int sx;
	int sy;
	int dx;
	int dy;
	
	dx = x2 - x1;
	ax = abs(dx) << 1;
	sx = sign(dx);
	
	dy = y2 - y1;
	ay = abs(dy) << 1;
	sy = sign(dy);
	
	x = x1;
	y = y1;
	
	if(ax > ay) {
		d = ay - (ax >> 1);
		for(;;) {
			put_pixel(scrn, x, y, color);
			if(x == x2)
				return 0;
			if(d >= 0) {
				y += sy;
				d -= ax;
			}
			x += sx;
			d += ay;
		}
	}else{
		d = ax - (ay >> 1);
		for(;;) {
			put_pixel(scrn, x, y, color);
			if(y == y2)
				return 0;
			if(d >= 0) {
				x += sx;
				d -= ay;
			}
			y += sy;
			d += ax;
		}
	}	
	
	return 1;
}

void Video_Line(int x1, int y1, int x2, int y2)
{
#define GRANULARITY	512
	if((x1 > SCREEN_W) && (x2 > SCREEN_W)) {
		return;
	}
	if((y1 > SCREEN_H) && (y2 > SCREEN_H)) {
		return;
	}
	if((x1 < 0) && (x2 < 0)) {
		return;
	}
	if((y1 < 0) && (y2 < 0)) {
		return;
	}
	draw_line(SCREEN_DATA, x1, y1, x2, y2, 0xFFFF);
}

void Video_Update()
{
	swiWaitForVBlank();
	u16* bd = bgData;
	u16* sd = SCREEN_DATA;
	int i;
	for(i = 0; i < BACK_BUFFER_SIZE; bd++, sd++, i++)
		*bd = *sd;
	Video_Clear();
}

void Video_Clear()
{
	memset(SCREEN_DATA, 0x00, SCREEN_W * SCREEN_H * 2);
}

