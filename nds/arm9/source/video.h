/*****************************************************************************
 *  video.h                                                                  *
 *  Video Abstraction functions.                                             *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2009 SquidMan (Alex Marshall)       <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#ifndef _VIDEO_H__
#define _VIDEO_H__

#ifdef __cplusplus
extern "C" {
#endif

extern int SCREEN_W, SCREEN_H;

int  Video_Init(int w, int h);
void Video_Quit();
void Video_Line(int x1, int y1, int x2, int y2);
void Video_Update();
void Video_Clear();
int  Event_Handle();

#ifdef __cplusplus
};
#endif

#endif /* _VIDEO_H__ */
