/*****************************************************************************
 *  video.h                                                                  *
 *  Video Abstraction functions.                                             *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2009-2010 trap15 (Alex Marshall)    <SquidMan72@gmail.com>  *
 *  Protected under the GNU GPLv2.                                           *
 *****************************************************************************/

#ifndef _VIDEO_H__
#define _VIDEO_H__

#include <stdint.h>

typedef uint8_t		u8;
typedef uint16_t	u16;
typedef uint32_t	u32;
typedef int8_t		s8;
typedef int16_t		s16;
typedef int32_t		s32;

int  Video_Init(int w, int h, int stereoscopic);
void Video_Quit();
void Video_Draw(double coords[3][6], u32 fill);
void Video_Wires(double coords[3][6], u32 fill);
void Video_Update();
void Video_Clear();
int  Event_Handle();

#endif /* _VIDEO_H__ */
